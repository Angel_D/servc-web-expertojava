/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paquete;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author mendo
 */
@WebService(serviceName = "Calculadora")
@Stateless()
public class Calculadora {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "sumar")
    public String sumar(@WebParam(name = "a") int a, @WebParam(name = "b") int b) {
        if(a < 0 || b < 0) { 
            return "Los valores deben ser enteros y positivos";
        } else {
            return "R = " + a + b;
        }
    }
}
